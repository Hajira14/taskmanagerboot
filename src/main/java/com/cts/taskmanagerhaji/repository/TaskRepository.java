package com.cts.taskmanagerhaji.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cts.taskmanagerhaji.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long>{
	public Task findBytaskId(long id);
}
