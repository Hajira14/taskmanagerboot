package com.cts.taskmanagerhaji;

//import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskmanagerhajiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskmanagerhajiApplication.class, args);
	
	}

}

