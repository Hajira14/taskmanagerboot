package com.cts.taskmanagerhaji.service;

import java.util.List;
//import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cts.taskmanagerhaji.model.Task;
import com.cts.taskmanagerhaji.repository.TaskRepository;

@Service
public class TaskService {
	
	@Autowired
    private TaskRepository taskRepository;
	public Task addTask(Task task) {
		 return taskRepository.save(task);
	}
	
	public List<Task> getTasks(){
		List<Task> task = taskRepository.findAll();
		return task;
	}
	
	
	public Task updateTask(Task task) {
		return taskRepository.save(task);
	}
	
	public Task getTaskById(long id) {
		
		return  taskRepository.findBytaskId(id);
	}
}
